import React, { Component } from 'react';
import { FlatList, Alert, TimePickerAndroid, DatePickerAndroid, Platform, DatePickerIOS, TextInput, Picker, Button, ActivityIndicator, Text, View, StyleSheet, Image, TouchableOpacity, Modal, AsyncStorage } from 'react-native';

var medicatie;
var weekDays = ["Ma", "Di", "Wo", "Do", "Vr", "Za", "Zo"];

export default class Add extends Component {

  constructor(props){
    super(props);
    this.setModalVisible = this.setModalVisible.bind(this);
    
    this.setDate = this.setDate.bind(this);
  }

  state = {
    modalVisible: false,
    isLoading: true,
    id: '',
    name: '',
    brand: '',
    inname: 'day',
    text: 'Search',
    chosenWeekDay: 'Ma',
    chosenDate: new Date(),
    chosenAndroidTime: '00:00',
    androidDate: `${new Date().getUTCDate()}/${new Date().getUTCMonth() + 1}/${new Date().getUTCFullYear()}`,
    value: 50,
  };

  setDate(newDate) {
    this.setState({chosenDate: newDate});
  }

  setDateAndroid = async () => {
    try {
      const {
        action, year, month, day,
      } = await DatePickerAndroid.open({
      date: new Date(),
      minDate: new Date(),
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({ androidDate: `${day}/${month + 1}/${year}` });
        this.state.androidDate = `${day}/${month + 1}/${year}`;
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  };
  
  setTimeAndroid = async () => {
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        hour: 14,
        minute: 0,
        is24Hour: false, // Will display '2 PM'
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        // Selected hour (0-23), minute (0-59)
        const m = (minute < 10) ? `0${minute}` : minute;
        const h = (hour < 10) ? `0${hour}` : hour;
        console.log(`time: ${hour}:${minute}`);
        this.setState({ chosenAndroidTime: `${h}:${m}` });
        this.state.chosenAndroidTime = `${h}:${m}`;
      }
    } catch ({ code, message }) {
      console.warn('Cannot open time picker', message);
    }
  };

  componentDidMount(){
    //https://www.kinderformularium.nl/geneesmiddelen.json
    medicatie = fetch('https://www.kinderformularium.nl/geneesmiddelen.json')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
      return medicatie;
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  openAddWindow(id, name, brand) {
    this.state.id = id;
    this.state.name = name;
    this.state.brand = brand;
    this.setModalVisible(!this.state.modalVisible);
  }

  datePicker = (choice) => {
    this.state.inname = choice;
    this.setState({inname: choice});
  }

  chooseWeekDay(day) {
    this.state.chosenWeekDay = day['r'];
    this.setDate(this.state.chosenDate);
  }

  saveItem = async () => {
    const value = await AsyncStorage.getItem('medicines');
    if (value !== null) {
      var items = JSON.parse(value);
    }
    else {
      this.resumeSave(value);
    }
    var canSave = true;
    items.map(r =>{
      if(r.id == this.state.id){
        //Alert.alert('Deze medicatie is al geselecteerd')
        console.log('alert')
        canSave = false
      }
    });
    if(canSave == true) {
      this.resumeSave(value);
    }
  }

  resumeSave = async (value) => {
    if (value !== null) {
      var items = JSON.parse(value);
    }
    else {
      var items = [];
    }
    arrayFiller = {};
    var item = {};
    Platform.OS === 'ios' ?
      item = {
        id: this.state.id,
        name: this.state.name,
        brand: this.state.brand,
        inname: this.state.inname,
        chosenWeekDay: this.state.chosenWeekDay,
        chosenDate: this.state.chosenDate,
        notes: 'Type je notities hier....',
      }
    : 
      item = {
        id: this.state.id,
        name: this.state.name,
        brand: this.state.brand,
        inname: this.state.inname,
        chosenWeekDay: this.state.chosenWeekDay,
        androidDate: this.state.androidDate,
        chosenAndroidTime: this.state.chosenAndroidTime,
        notes: 'Type je notities hier....',
      }
    ;
    items.push(item);
    try {
      //console.log(item);
      //console.log(items);
      await AsyncStorage.setItem('medicines', JSON.stringify(items));
      //console.log('Help: ' + await AsyncStorage.getItem('medicines'));
    } catch (error) {
      console.log('error');
    }
    this.setModalVisible(!this.state.modalVisible)
  }

  inputDate() {
    switch (this.state.inname) {
      case 'day':
        return (
          Platform.OS === 'ios' ? (
            <DatePickerIOS
              mode='time'
              date={this.state.chosenDate}
              onDateChange={this.setDate}
            />
          ) : (
            <View>
              <TouchableOpacity onPress={() => this.setTimeAndroid()}>
                <View>
                  <Text style={{ fontSize: 16 }}>
                    {this.state.chosenAndroidTime}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )
        );
      case 'week':
        return (
          Platform.OS === 'ios' ? (
            <View>
              <View style={styles.weekDays}>
                {weekDays.map(r => 
                  r === this.state.chosenWeekDay ? (
                    <TouchableOpacity style={styles.weekDayChosen} onPress={() => this.chooseWeekDay({r})}> 
                      <Text>{r}</Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity style={styles.weekDay} onPress={() => this.chooseWeekDay({r})}> 
                      <Text>{r}</Text>
                    </TouchableOpacity>
                  )
                )
                }
              </View>
              <DatePickerIOS
                mode='time'
                date={this.state.chosenDate}
                onDateChange={this.setDate}
              />
            </View>
          ) : (
            <View>
              <View style={styles.weekDays}>
              {weekDays.map(r =>
                  r === this.state.chosenWeekDay ? (
                    <TouchableOpacity style={styles.weekDayChosen} onPress={() => this.chooseWeekDay({r})}> 
                      <Text>{r}</Text>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity style={styles.weekDay} onPress={() => this.chooseWeekDay({r})}> 
                      <Text>{r}</Text>
                    </TouchableOpacity>
                  )
              )
                }
              </View>
              <TouchableOpacity onPress={() => this.setTimeAndroid()}>
                <View>
                  <Text style={{ fontSize: 16 }}>
                    {this.state.chosenAndroidTime}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )
        );
      case 'custom':
        return (
          Platform.OS === 'ios' ? (
            <DatePickerIOS
              date={this.state.chosenDate}
              onDateChange={this.setDate}
            />
          ) : (
            <View>
              <TouchableOpacity onPress={() => this.setDateAndroid()}>
                <View >
                  <Text style={{ fontSize: 16 }}>
                    {this.state.androidDate}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.setTimeAndroid()}>
                <View>
                  <Text style={{ fontSize: 16 }}>
                    {this.state.chosenAndroidTime}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )
        );
      default:
        return (
          console.log('error')
        );
    }
  }

  search() {
    var text = this.state.text;
    var output = [];
    medicatie = fetch('https://www.kinderformularium.nl/geneesmiddelen.json')
      .then((response) => response.json())
      .then((responseJson) => {
        if(!text) {
          this.setState({
            isLoading: false,
            dataSource: responseJson,
          }, function(){
  
          });
        }
        else{
          for (let singleItem of responseJson) {
            if(singleItem.generic_name == text || singleItem.generic_name.includes(text) || singleItem.branded_name == text || singleItem.generic_name.includes(text)) {
              output.push(singleItem);
            }
          }
          this.setState({
            isLoading: false,
            dataSource: output,
          }, function(){
  
          });
        }
      })
      .catch((error) =>{
        console.error(error);
      });
      return medicatie;
  }

  render(){
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          >
          <View style={styles.modal}>
            <View>
              <Text style={styles.modalText}>{this.state.name}</Text>
              <Text style={styles.modalText}>{this.state.brand}</Text>
            </View>
            <Text></Text>
            <Text>Wanneer innemen?</Text>
            <Picker
              selectedValue={this.state.inname}
              onValueChange={(itemValue) => this.datePicker(itemValue)}>
              <Picker.Item label="Per dag" value="day" />
              <Picker.Item label="Per week" value="week" />
              <Picker.Item label="Aangepast" value="custom" />
            </Picker>
            
            {this.inputDate()}

          </View>
          <Button
            onPress={ () => this.saveItem()}
            title='Voeg toe'
          />
          <Button
            onPress={ () => this.setModalVisible(!this.state.modalVisible)}
            title='Sluit'
          />
        </Modal>
        <View>
          <TextInput
            style={styles.search}
            onChangeText={(text) => this.setState({text})}
            placeholder='Search'
          />
          <Button
              style={styles.modalClose}
              onPress={ () => this.search()}
              title='Search'
            />
        </View>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => 
            <TouchableOpacity style={styles.addItem} onPress={ () => this.openAddWindow(item.id, item.generic_name, item.branded_name)}>
              <Text style={styles.addItemText}>
                <Image
                  style={{width: 30, height: 30}}
                  source={require('../assets/meds.png')}
                />
                {item.generic_name}, {item.branded_name}
              </Text>
            </TouchableOpacity>}
          keyExtractor={({id}) => id.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  addItem: {
    padding: 50,
    paddingTop: 70,
    paddingBottom: 70,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  addItemText: {
    fontSize: 20,
  },
  modal: {
    margin: 50,
    marginTop: 70,
  },
  modalText: {
    fontSize: 20,
  },
  search: {
    marginTop: -17,
    padding: 10,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  weekDays: {
    flexDirection: 'row',
  },
  weekDay: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
    flex: 1,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  weekDayChosen: {
    backgroundColor: "rgb(192, 242, 194)",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
    flex: 1,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  }
});