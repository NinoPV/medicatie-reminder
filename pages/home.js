import React, { Component } from 'react';
import { ScrollView, RefreshControl, Text, TextInput, Image, Platform, Button, FlatList, ActivityIndicator, View, StyleSheet, AsyncStorage, TouchableOpacity, Modal } from 'react-native';

export default class Home extends Component {

  state = {
    isLoading: true,
    dataSource: [],
    id: '',
    name: '',
    brand: '',
    inname: '',
    weekDay: '',
    chosenDate: '',
    chosenTime: '',
    androidDate: '',
    chosenAndroidTime: '',
    modalVisible: false,
    text: '',
    refreshing: false,
    results: '',
  };

  componentDidMount() {
    this.getItems();
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.getItems().then(() => {
      this.setState({refreshing: false});
    });
  }

  getItems = async () => {
    try {
      const value = await AsyncStorage.getItem('medicines');
      console.log(value.length);
      if(value.length == 2) {
        this.state.results = "Geen resultaten gevonden";
      }
      if(value !== null) {
        //this.state.results = null;
        const allMeds = JSON.parse(value);
        console.log(value);
        this.setState({
          dataSource: allMeds,
          isLoading: false
        });
      }
      this.setState({
        isLoading: false,
      });
    } catch (error) {
      console.log('error Get Items');
    }
  }

  deleteItems = async () => {
    try {
      AsyncStorage.clear();
      output = [];
      this.state.dataSource = [];
      this.getItems();
    } catch (error) {
      // Error retrieving data
      console.log(error.message);
    }
  }

  openAddWindow(id, gen_name, bran_name, inname, weekDay, chosenDate, androidDate, chosenAndroidTime, notes) {
    this.state.id = id;
    this.state.name = gen_name;
    this.state.brand = bran_name;
    this.state.inname = inname;
    this.state.weekDay = weekDay;
    this.state.text = notes;
    this.setModalVisible(!this.state.modalVisible);

    if(Platform.OS == "ios") {
      var string = chosenDate.split("T");
      var nextString = string[1].split(":");
      if(nextString[0] == "23") {
        nextString[0] = "-01";
      }
      var lastString = JSON.stringify(parseInt(nextString[0]) + 1) + ':' + nextString[1];

      this.state.chosenDate = chosenDate;
      this.state.chosenTime = lastString;
    }
    else {
      this.state.androidDate = androidDate;
      this.state.chosenAndroidTime = chosenAndroidTime;
    }
  }

  splitTimeIOS(date) {
    var string = date.split("T");
      var nextString = string[1].split(":");
      if(nextString[0] == "23") {
        nextString[0] = "-01";
      }
    var lastString = JSON.stringify(parseInt(nextString[0]) + 1) + ':' + nextString[1];
    return lastString;
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  save = async () => {
    this.changeNotes(this.state.id, this.state.text);
    try {
      AsyncStorage.clear();
      await AsyncStorage.setItem('medicines', JSON.stringify(this.state.dataSource));
    } catch (error) {
      console.log('error');
    }
    this.setModalVisible(!this.state.modalVisible);
  }

  changeNotes( value, note ) {
    for (var i in this.state.dataSource) {
      if (this.state.dataSource[i].id == value) {
        this.state.dataSource[i].notes = note;
        break;
      }
    }
  }

  verwijder = async () => {
    this.state.dataSource.map(r =>{
      if(r.id == this.state.id){
        this.state.dataSource.splice(this.state.dataSource.indexOf(r), 1)
      }
      else {
        console.log('nothing')
      }
    });
     try {
      await AsyncStorage.setItem('medicines', JSON.stringify(this.state.dataSource));
    } catch (error) {
      console.log('error');
    }
    this.setModalVisible(!this.state.modalVisible);
    this.getItems();
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    
    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }>
      <View>
        <View style={styles.resultWrapper}>
          <Text style={styles.noResult}>{this.state.results}</Text>
        </View>
        {
          this.state.inname === 'day' ? (
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
            >
              <View style={styles.modal}>
                <View>
                  <Text style={styles.modalText}>{this.state.name}</Text>
                  <Text style={styles.modalText}>{this.state.brand}</Text>
                  <View style={styles.modalBlock}>
                  {
                  Platform.OS === 'ios' ? (
                    <Text style={styles.blockText}>Innemen per dag om {this.state.chosenTime}</Text>
                  ) : (
                    <Text style={styles.blockText}>Innemen per dag om {this.state.chosenAndroidTime}</Text>
                  )
                  }
                  <Text></Text>
                  <Text>Voeg hier notities bij indien nodig</Text>
                  <Text></Text>
                  <View style={{
                    backgroundColor: this.state.text,
                    borderBottomColor: '#000000',
                    borderBottomWidth: 1 }}
                  >
                    <TextInput
                      multiline = {true}
                      numberOfLines = {4}
                      onChangeText={(text) => this.setState({text})}
                      value={this.state.text}
                    />
                  </View>
                  </View>
                </View>
              <Text></Text>
              </View>
              <Button
                onPress={ () => this.save()}
                title='Sla notities op'
              />
              <Button
                onPress={ () => this.verwijder()}
                title='Verwijder'
              />
              <Button
                onPress={ () => this.setModalVisible(!this.state.modalVisible)}
                title='Sluit'
              />
            </Modal>
          ) : (
            this.state.inname === 'week' ? (
              <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
            >
              <View style={styles.modal}>
                <View>
                  <Text style={styles.modalText}>{this.state.name}</Text>
                  <Text style={styles.modalText}>{this.state.brand}</Text>
                  <View style={styles.modalBlock}>
                  {Platform.OS === 'ios' ? (
                    <Text style={styles.blockText}>Innemen per week op {this.state.weekDay} om {this.state.chosenTime}</Text>
                  ) : (
                    <Text style={styles.blockText}>Innemen per week op {this.state.weekDay} om {this.state.chosenAndroidTime}</Text>
                  )
                  }
                  <Text></Text>
                  <Text>Voeg hier notities bij indien nodig</Text>
                  <Text></Text>
                  <View style={{
                    backgroundColor: this.state.text,
                    borderBottomColor: '#000000',
                    borderBottomWidth: 1 }}
                  >
                    <TextInput
                      multiline = {true}
                      numberOfLines = {4}
                      onChangeText={(text) => this.setState({text})}
                      value={this.state.text}
                    />
                  </View>
                  </View>
                </View>
              <Text></Text>
              </View>
              <Button
                onPress={ () => this.save()}
                title='Sla notities op'
              />
              <Button
                onPress={ () => this.verwijder()}
                title='Verwijder'
              />
              <Button
                onPress={ () => this.setModalVisible(!this.state.modalVisible)}
                title='Sluit'
              />
            </Modal>
            ) : (
              console.log()
            )
          )
        }

          <FlatList
            data={this.state.dataSource}
            renderItem={({item}) => 
              <TouchableOpacity style={styles.addItem} onPress={ () => this.openAddWindow(item.id, item.name, item.brand, item.inname, item.chosenWeekDay, item.chosenDate, item.androidDate, item.chosenAndroidTime, item.notes)}>
                  <Image
                    style={{width: 30, height: 30}}
                    source={require('../assets/meds.png')}
                  />
                  {
                    Platform.OS === 'ios' ? (
                      <Text style={styles.addItemText}>{item.name}, {item.brand}    {this.splitTimeIOS(item.chosenDate)}</Text>
                    ):(
                      <Text style={styles.addItemText}>{item.name}, {item.brand}    {item.chosenAndroidTime}</Text>
                    )
                  }
              </TouchableOpacity>}
            keyExtractor={({id}) => id.toString()}
          />
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  addItem: {
    padding: 50,
    paddingTop: 70,
    paddingBottom: 70,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    flexDirection:'row',
  },
  addItemText: {
    fontSize: 20,
  },
  modal: {
    margin: 50,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalBlock: {
    margin: 50,
    marginTop: 70,
    minWidth: 400,
    alignItems: 'center'
  },
  modalText: {
    fontSize: 20,
    marginLeft: 100
  },
  blockText: {
    fontSize: 20,
  },
  noResult: {
    top: 100,
    fontSize: 20,
    color: 'rgb(149, 149, 149)',
  },
  resultWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  }
});