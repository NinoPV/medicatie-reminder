import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import Home from './pages/home';
import Add from './pages/add';

var page = 'Home';

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.changePage = this.changePage.bind(this);
  }
  state = {
    Page: 'Home',
  }

  renderElement() {
    if(page == 'Home') {
      return <Home />
    }
    else{
      return <Add />
    }
  }

  changePage(input) {
    page = input;
    this.setState({
      Page: page
    })
    this.renderElement();
  }


  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
        { this.renderElement() }
        </View>
        <View style={styles.navbar}>
          <TouchableOpacity onPress={ () => this.changePage('Home')}>
            <Image
              style={{width: 30, height: 30}}
              source={require('./assets/home.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={ () => this.changePage('Add')}>
            <Image
              style={{width: 30, height: 30}}
              source={require('./assets/add.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  help: {
    paddingBottom: 50,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    paddingTop: 50,
  },
  navbar : {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 80,
    paddingRight: 80,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    backgroundColor: '#fafafa'
  },
});
